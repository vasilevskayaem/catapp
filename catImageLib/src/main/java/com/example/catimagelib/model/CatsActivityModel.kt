package com.example.catimagelib.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.catimagelib.network.CatApi
import com.example.catimagelib.network.CatObject
import com.example.catimagelib.network.DEFAULT_LIMIT
import com.example.catimagelib.network.DEFAULT_PAGE
import kotlinx.coroutines.launch

enum class CatApiStatus { LOADING, ERROR, DONE }

/**
 * The [ViewModel] that is attached to the CatsActivity.
 */
class CatsActivityModel : ViewModel() {

    /**
     * status of response from sercer side
     */
    private val _status = MutableLiveData<CatApiStatus>()

    val status: MutableLiveData<CatApiStatus>
        get() = _status

    /**
     * collection of cats objects returned from server side
     */
    private val _cats = MutableLiveData<List<CatObject>>()

    val cats: LiveData<List<CatObject>>
        get() = _cats

    private val _navigateToSelectedCat = MutableLiveData<CatObject>()
    val navigateToSelectedCat: LiveData<CatObject>
        get() = _navigateToSelectedCat

    /**
     * start loading of the data immediately after ViewModel was created
     */
    init {
        getCatsImages()
    }


    /**
     * [getCatsImages] initiate downloading from server side. The Retrofit service
     * returns a coroutine
     */
    fun getCatsImages() {
        viewModelScope.launch {
            _status.value = CatApiStatus.LOADING
            try {
                _cats.value = CatApi.retrofitService.searchCats(DEFAULT_LIMIT, DEFAULT_PAGE)
                _status.value = CatApiStatus.DONE
            } catch (e: Exception) {
                _status.value = CatApiStatus.ERROR
                _cats.value = ArrayList()
            }
        }
    }

    /**
     * when user clicks on cat image this method update LiveDate, CatActivity observes liveDate and
     * returns imageUrl as activityResult
     */
    fun displayCatDetails(catObject: CatObject) {
        _navigateToSelectedCat.value = catObject
    }

}
