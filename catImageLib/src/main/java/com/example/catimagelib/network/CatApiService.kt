package com.example.catimagelib.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


private const val BASE_URL = "https://api.thecatapi.com"
private const val API_KEY = "f56210f8-85d3-453f-ac88-c442fb4fa874"
/**
 * hardcoded values for network request, in general here can be implemented pagination instead of
 * hardcoding
 */
const val DEFAULT_LIMIT = 20
const val DEFAULT_PAGE = 1

/**
 * [moshi] used for json parsing
 */
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()


private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

/**
 * A public interface that exposes the [searchCats] method
 */
interface CatApiService{

    @Headers("x-api-key:${API_KEY}")
    @GET("v1/images/search")
    suspend fun searchCats(
        @Query("limit") limit: Int,
        @Query("page") page: Int,
    ): List<CatObject>
}

/**
 * A public Api object that exposes the lazy-initialized Retrofit service
 */
object CatApi {
    val retrofitService : CatApiService by lazy { retrofit.create(CatApiService::class.java) }
}
