package com.example.catimagelib.network

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * This data class defines a cat
 */
@Parcelize
data class CatObject (
    val id: String,
    val url: String):
    Parcelable