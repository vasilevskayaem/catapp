package com.example.catimagelib

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.catimagelib.adapter.CatAdapter
import com.example.catimagelib.databinding.ActivityCatsBinding
import com.example.catimagelib.model.CatsActivityModel


class CatsActivity : AppCompatActivity() {

    companion object {
        /**
         * result code that will be returned to parent activity
         */
        const val GET_CAT_IMAGE_RESPONSE_OK = 12345

        /**
         * name of Extra where will be stored selected cat url
         */
        const val CAT_IMAGE_URL = "CAT_IMAGE_URL"
    }

    private val viewModel: CatsActivityModel by lazy {
        ViewModelProvider(this).get(CatsActivityModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityCatsBinding = DataBindingUtil.setContentView(
            this, R.layout.activity_cats)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.catRecyclerView.adapter = CatAdapter(CatAdapter.OnClickListener {
            viewModel.displayCatDetails(it)
        })
        // observe LiveData changes in ViewModel, return image url when user select a cat
        viewModel.navigateToSelectedCat.observe(this, Observer {
            if(it!=null){
                    val data = Intent().apply {
                        putExtra(CAT_IMAGE_URL, it.url)
                    }
                    setResult(GET_CAT_IMAGE_RESPONSE_OK, data)
                    finish()
            }
        })

    }

}