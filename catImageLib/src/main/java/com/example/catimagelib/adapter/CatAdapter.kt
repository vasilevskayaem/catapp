package com.example.catimagelib.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.catimagelib.databinding.CatViewItemBinding
import com.example.catimagelib.network.CatObject
/**
 * adapter for RecyclerView on CatActivity
 */
class CatAdapter( private val onClickListener: OnClickListener ) :
    ListAdapter<CatObject,
            CatAdapter.CatViewHolder>(DiffCallback) {

    class CatViewHolder(private var binding: CatViewItemBinding):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(catObject: CatObject) {
            binding.cat = catObject
            binding.executePendingBindings()
        }
    }


    companion object DiffCallback : DiffUtil.ItemCallback<CatObject>() {
        override fun areItemsTheSame(oldItem: CatObject, newItem: CatObject): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: CatObject, newItem: CatObject): Boolean {
            return oldItem.id == newItem.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): CatViewHolder {
        return CatViewHolder(CatViewItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        val catObject = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(catObject)
        }
        holder.bind(catObject)
    }

    class OnClickListener(val clickListener: (catObject: CatObject) -> Unit) {
        fun onClick(catObject: CatObject) = clickListener(catObject)
    }
}

