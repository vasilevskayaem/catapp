/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.catimagelib.adapter

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.catimagelib.R
import com.example.catimagelib.model.CatApiStatus
import com.example.catimagelib.network.CatObject

/**
 * put downloaded cat image collection from model to recyclerView
 */
@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<CatObject>?) {
    val adapter = recyclerView.adapter as CatAdapter
    adapter.submitList(data)
}

/**
 * download image grids of recycledView
 */
@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context)
            .load(imgUri)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.ic_baseline_broken_image_24)
            )
            .into(imgView)
    }
}

/**
 * shows or hides an error message depending on the network response status
 */
@BindingAdapter("catApiStatus")
fun bindStatus(statusTextView: TextView, status: CatApiStatus?) {
    when (status) {
        CatApiStatus.LOADING -> {
            statusTextView.visibility = View.VISIBLE
            statusTextView.setText(statusTextView.context.getString(R.string.text_loading))
        }
        CatApiStatus.ERROR -> {
            statusTextView.visibility = View.VISIBLE
            statusTextView.setText(statusTextView.context.getString(R.string.text_error))
        }
        CatApiStatus.DONE -> {
            statusTextView.visibility = View.GONE
        }
    }
}

/**
 * shows or hides an "retry" button depending on the network response status
 */
@BindingAdapter("catApiRetry")
fun bindButton(button: Button, status: CatApiStatus?) {
    when (status) {
        CatApiStatus.ERROR -> {
            button.visibility = View.VISIBLE
        }
        else -> {
            button.visibility = View.GONE
        }
    }
}
