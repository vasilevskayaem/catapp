package com.example.catimagelib.model
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.catimagelib.network.CatObject
import junit.framework.TestCase
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CatsActivityModelTest : TestCase() {
    private lateinit var catsActivityModel: CatsActivityModel

    @Before
    fun setupViewModel() {
        catsActivityModel = CatsActivityModel()
    }


    @Test
    fun testDisplayCatDetails() {
        catsActivityModel.displayCatDetails(CatObject("1", "https://test.com"))
        val value = catsActivityModel.navigateToSelectedCat.value
        Assert.assertEquals(
                value?.id,
                "1"
        )
    }
}