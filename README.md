# CatApp

**The skills challenge is:**

Create an Android library to pick a cat image from http://thecatapi.com/.
The library shows one screen where the user can scroll through cat pictures. When the user taps on a cat picture the library closes the screen and returns the selected picture. The screen’s action bar shows a back button and a title.
The library should allow customization of the action bar’s color, title, and back button.

**app** module is just simple android app that uses **catImageLib** library to pick cat image.

**How to use library (now already implemented second approach, first approach I use locally to observe changes in library immediately):**

1) it can be used as library module. Just write in **app** gradle file `implementation project(path: ':catImageLib')`. This line is commented.
2) generate aar file and use aar file. It's already done, folder app/repo contains aar and pom file for this. In project gradle file add local maven repository `maven {url "repo"}`, in **app** gradle file add implementation `('com.example:catimagelib:0.0.1@aar'){transitive = true}`

**How to generate library (already done, but after some changes in library code if app uses aar file, it should be completely regenerated):**

Run for **catImageLib** next gradle tasks : `assembleRelease` -- generates pure aar file, `publish` -- creates pom file for dependencies, and put aar file to `app/repo` directory. So now library is published in local maven repository.

**How to customize library**

1) put ic_cat_activity_home_indicator (mdpi, hdpi, xhdpi, xxhdpi, xxxhdpi) in your drawable folder of your project, it will be used instead of default back button 
2) override colors "cat_activity_action_bar_color_dark" and "cat_activity_action_bar_color" for action bar
3) override string "cat_activity_action_bar_title" for title

**How to use library**

1) import library in gradle file of your project
2) add `com.example.catimagelib.CatsActivity` activity in your manifest file
3) start CatsActivity for result
4) observe result. Response code should be `com.example.catimagelib.CatsActivity#GET_CAT_IMAGE_RESPONSE_OK = 12345` and image url you can find in returned intent in extra String with name `com.example.catimagelib.CatsActivity#CAT_IMAGE_URL = "CAT_IMAGE_URL"`

In library I use android ViewModel and LiveDate for interaction between repository (Cat api network calls) and view. Android Databindng is used for populatind date from ViewModel to UI. Retrofit is used for network communication. Glide library is used for downloading images.






