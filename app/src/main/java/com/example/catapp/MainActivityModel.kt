package com.example.catapp
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityModel : ViewModel() {

    /**
     * [_url] of the selected image, will be uploaded with databinding in @
     */
    private val _url = MutableLiveData<String>()

    val url: MutableLiveData<String>
        get() = _url

}