package com.example.catapp

import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * [catImage] method load image in imageView
 */
@BindingAdapter("catUrl")
fun catImage(imgView: ImageView, catUrl: String?) {
    catUrl?.let {
        val imgUri = catUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context)
            .load(imgUri)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.ic_cat_placeholder)
            )
            .into(imgView)
    }
}
