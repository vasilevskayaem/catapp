package com.example.catapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.catapp.databinding.ActivityMainBinding
import com.example.catimagelib.CatsActivity
import com.example.catimagelib.CatsActivity.Companion.CAT_IMAGE_URL
import com.example.catimagelib.CatsActivity.Companion.GET_CAT_IMAGE_RESPONSE_OK

class MainActivity : AppCompatActivity() {
    companion object {
        const val GET_CAT_IMAGE_REQUEST_CODE = 123
    }

    private val viewModel: MainActivityModel by lazy {
        ViewModelProvider(this).get(MainActivityModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(
            this, R.layout.activity_main
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
    }

    /**
     * [pickCat] start CatsActivity to pick a image
     */
    fun pickCat(view: View) {
        val intent = Intent(this, CatsActivity::class.java)
        startActivityForResult(intent, GET_CAT_IMAGE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == GET_CAT_IMAGE_RESPONSE_OK) {
            viewModel.url.value = data?.getStringExtra(CAT_IMAGE_URL)
        }
    }

}